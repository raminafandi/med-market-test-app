import {StyleSheet, Text} from 'react-native';
import React, {useCallback} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import Button from '../components/Button';
import Colors from '../constants/Colors';
import ResultStore from '../store/ResultStore';
import times from '../constants/Times';

const DetailScreen = ({route, navigation}: any) => {
  const {item} = route.params;

  const onDelete = useCallback(() => {
    ResultStore.deleteResult(item.id);
    navigation.goBack();
  }, [item.id, navigation]);

  return (
    <SafeAreaView style={styles.safe}>
      <Text style={styles.name}>{item.name}</Text>
      <Text style={styles.date}>{item.date}</Text>
      <Text style={styles.time}>
        {times.find(time => time.value === item.time)?.title || ''}
      </Text>
      <Text style={styles.description}>{item.description}</Text>
      <Button title="Delete" onPress={onDelete} color={Colors.red} />
    </SafeAreaView>
  );
};

export default DetailScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    padding: 20,
    backgroundColor: Colors.background,
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    color: Colors.black,
  },
  date: {
    fontSize: 18,
    color: Colors.black,
    marginBottom: 10,
  },
  time: {
    fontSize: 18,
    color: Colors.black,
    marginBottom: 10,
  },
  description: {
    fontSize: 18,
    color: Colors.black,
    marginBottom: 10,
  },
});
