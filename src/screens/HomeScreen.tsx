import {FlatList, Pressable, StyleSheet, Text, View} from 'react-native';
import React, {useCallback} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import ListItem from '../components/ListItem';
import SwipeableModal from '../components/SwipeableModal';
import Colors from '../constants/Colors';
import {observer} from 'mobx-react';
import ResultStore from '../store/ResultStore';
import DateTimePicker, {
  DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import Button from '../components/Button';
import {Result} from '../types';
import Strings from '../constants/Strings';

const HomeScreen = observer(({navigation}: any) => {
  const [isModalVisible, setIsModalVisible] = React.useState<boolean>(false);
  const [sorted, setSorted] = React.useState<boolean>(false);
  const [filtered, setFiltered] = React.useState<boolean>(false);
  const [initialDate, setInitialDate] = React.useState<Date>(new Date());
  const [finalDate, setFinalDate] = React.useState<Date>(new Date());
  const [iShow, setIShow] = React.useState<boolean>(false);
  const [fShow, setFShow] = React.useState<boolean>(false);

  const sortByDate = useCallback(
    (array: Result[]) =>
      array
        .slice()
        .sort(
          (a: any, b: any) =>
            new Date(b.date).getTime() - new Date(a.date).getTime(),
        ),
    [],
  );

  console.log('data:', ResultStore.getResults());

  const onChangeInitial = useCallback(
    (event: DateTimePickerEvent, date: Date) => {
      const currentDate = date || initialDate;
      setIShow(false);
      setInitialDate(currentDate);
    },
    [initialDate],
  );

  const onChangeFinal = useCallback(
    (event: DateTimePickerEvent, date: Date) => {
      const currentDate = date || finalDate;
      setFShow(false);
      setFinalDate(currentDate);
    },
    [finalDate],
  );

  const changeFilter = useCallback((flag: boolean) => {
    setFiltered(flag);
    setIsModalVisible(false);
  }, []);

  return (
    <SafeAreaView style={styles.safe}>
      <View style={styles.topContainer}>
        <Text
          style={[styles.sbutton, sorted && {backgroundColor: Colors.green}]}
          onPress={() => setSorted(!sorted)}>
          {Strings.sort}
        </Text>
        <Text
          style={[styles.sbutton, filtered && {backgroundColor: Colors.green}]}
          onPress={() => setIsModalVisible(!isModalVisible)}>
          {Strings.filter}
        </Text>
      </View>
      <FlatList
        data={
          filtered
            ? sorted
              ? sortByDate(ResultStore.filterByDate(initialDate, finalDate))
              : ResultStore.filterByDate(initialDate, finalDate)
            : sorted
            ? sortByDate(ResultStore.results)
            : ResultStore.results
        }
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <ListItem
            key={item.id}
            title={item.name}
            date={item.date.toDateString()}
            time={item.time}
            onDelete={() => ResultStore.deleteResult(item.id)}
            onPress={() =>
              navigation.navigate('Detail', {
                item: {...item, date: item.date.toDateString()},
              })
            }
          />
        )}
      />
      <SwipeableModal
        isVisible={isModalVisible}
        setIsVisible={setIsModalVisible}
        modalViewStyle={styles.modalStyle}>
        <Text style={styles.modalTitle}>{Strings.filter}</Text>
        <Text style={styles.modalDate}>{Strings.initialDate}</Text>
        <Pressable onPress={() => setIShow(!iShow)}>
          <Text style={styles.date}>
            {initialDate.toLocaleDateString('ru-RU', {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
            })}
          </Text>
        </Pressable>
        {iShow && (
          <DateTimePicker value={initialDate} onChange={onChangeInitial} />
        )}

        <Text style={styles.modalDate}>{Strings.finalDate}</Text>
        <Pressable onPress={() => setFShow(!fShow)}>
          <Text style={styles.date}>
            {finalDate.toLocaleDateString('ru-RU', {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
            })}
          </Text>
        </Pressable>
        {fShow && (
          <DateTimePicker value={initialDate} onChange={onChangeFinal} />
        )}

        <View style={styles.buttons}>
          <Button
            title={Strings.deleteFilters}
            color={Colors.red}
            onPress={() => changeFilter(false)}
          />
          <Button title={Strings.filter1} onPress={() => changeFilter(true)} />
        </View>
      </SwipeableModal>

      <Pressable
        style={styles.plusContainer}
        onPress={() => navigation.navigate('Add')}>
        <Text style={styles.plusText}>+</Text>
      </Pressable>
    </SafeAreaView>
  );
});

export default HomeScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    padding: 10,
    backgroundColor: Colors.background,
  },
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  sbutton: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    color: Colors.black,
    backgroundColor: Colors.background,
    borderColor: Colors.black,
    borderWidth: 0.5,
    borderRadius: 10,
  },
  plusContainer: {
    position: 'absolute',
    bottom: 30,
    right: 10,
    backgroundColor: Colors.green,
    width: 70,
    height: 70,
    borderRadius: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  plusText: {
    fontSize: 40,
    color: Colors.white,
  },
  modalStyle: {
    paddingTop: 60,
  },
  modalTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: Colors.black,
  },
  modalDate: {
    fontSize: 18,
    color: Colors.gray,
    marginVertical: 4,
  },
  buttons: {
    marginTop: 30,
  },
  date: {
    fontSize: 24,
    color: Colors.black,
  },
});
