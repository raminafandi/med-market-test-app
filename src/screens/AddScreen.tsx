import {Alert, Pressable, StyleSheet, Text, View} from 'react-native';
import React, {useCallback} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import Input from '../components/Input';
import Colors from '../constants/Colors';
import Radio from '../components/Radio';
import DateTimePicker, {
  DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import Button from '../components/Button';
import ResultStore from '../store/ResultStore';
import uuid from 'react-uuid';
import {observer} from 'mobx-react-lite';
import Strings from '../constants/Strings';
import times from '../constants/Times';
import {Time} from '../types';

const AddScreen = observer(({navigation}: any) => {
  const [time, setTime] = React.useState<Time>(times[0]);
  const [date, setDate] = React.useState<Date>(new Date());
  const [show, setShow] = React.useState<boolean>(false);
  const [name, setName] = React.useState<string>('');
  const [description, setDescription] = React.useState<string>('');

  const onChange = useCallback(
    (event: DateTimePickerEvent, selectedDate: Date) => {
      const currentDate: Date = selectedDate || date;
      setShow(false);
      setDate(currentDate);
    },
    [date],
  );

  const onSubmit = () => {
    if (name.length > 0) {
      ResultStore.createResult({
        id: uuid(),
        name,
        description,
        date,
        time: time.value,
        createdAt: new Date(),
      });
      navigation.navigate('Home');
    } else {
      Alert.alert(Strings.error, Strings.noNameError);
    }
  };

  return (
    <SafeAreaView style={styles.safe}>
      <View style={styles.container}>
        <Text style={styles.title}>{Strings.addNew}</Text>
        <Input placeholder={Strings.name} value={name} onChangeText={setName} />
        <Input
          placeholder={Strings.comment}
          value={description}
          onChangeText={setDescription}
          multiline={true}
          numberOfLines={4}
        />
        <View>
          <Text style={styles.timeText}>{Strings.selectTime}</Text>
          {times.map(item => (
            <Radio
              key={item.value}
              title={item.title}
              isActive={item.value === time.value}
              onPress={() => setTime(item)}
            />
          ))}
          <Text style={styles.timeText}>{Strings.selectDate}</Text>
          <Pressable onPress={() => setShow(!show)}>
            <Text style={styles.date}>
              {date.toLocaleDateString('ru-RU', {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
              })}
            </Text>
          </Pressable>
          {show && <DateTimePicker value={date} onChange={onChange} />}
          <Button title={Strings.save} onPress={() => onSubmit()} />
        </View>
      </View>
    </SafeAreaView>
  );
});

export default AddScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    color: Colors.black,
  },
  container: {
    flex: 1,
    padding: 16,
  },
  timeText: {
    fontSize: 18,
    color: Colors.black,
    marginVertical: 6,
  },
  date: {
    fontSize: 22,
    color: Colors.black,
  },
});
