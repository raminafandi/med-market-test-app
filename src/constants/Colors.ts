export default {
  background: '#f2f2f2',
  black: '#343434',
  white: '#fff',
  green: '#06d6a0',
  lighterGrey: '#f2f2f2',
  red: '#f13030',
  borderColor: '#dee2e6',
  shadowColor: '#000',
  gray: '#808080',
};
