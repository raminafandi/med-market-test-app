export default [
  {
    title: 'Утро',
    value: 'morning',
  },
  {
    title: 'День',
    value: 'day',
  },
  {
    title: 'Вечер',
    value: 'evening',
  },
];
