export type Result = {
  id: string;
  name: string;
  description: string;
  date: Date;
  time: string;
  createdAt: Date;
};

export type Time = {
  title: string;
  value: string;
};
