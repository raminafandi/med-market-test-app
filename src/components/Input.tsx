import * as React from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  ViewProps,
  TextInputProps,
} from 'react-native';

import Colors from '../constants/Colors';

type InputProps = ViewProps &
  TextInputProps & {
    iconName?: any;
    placeholder?: any;
    value?: any;
    inputStyle?: any;
  };

const Input = (props: InputProps) => {
  return (
    <View style={[styles.container, props.inputStyle && props.inputStyle]}>
      <TextInput
        placeholder={props.placeholder}
        placeholderTextColor={Colors.gray}
        style={styles.placeholder}
        value={props.value}
        {...props}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
    minheight: 60,
    borderRadius: 8,
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },
  placeholder: {
    fontSize: 16,
    color: Colors.black,
  },
});
