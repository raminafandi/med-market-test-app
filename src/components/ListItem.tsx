import {observer} from 'mobx-react-lite';
import React from 'react';
import {Pressable, PressableProps, StyleSheet, Text, View} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Colors from '../constants/Colors';
import Times from '../constants/Times';

type ListItemProps = PressableProps & {
  title: string;
  date: string;
  time: string;
  onDelete?: () => void;
};

const ListItem = observer(
  ({title, date, time, onDelete, ...props}: ListItemProps) => {
    return (
      <Pressable style={styles.container} {...props}>
        <View style={styles.leftSide}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.time}>
            {Times.find(item => item.value === time)?.title || ''}
          </Text>
          <Text style={styles.date}>{date}</Text>
        </View>
        <Pressable style={styles.deleteButton} onPress={onDelete}>
          <FontAwesome5 name={'trash'} brand size={16} color={'#fff'} />
        </Pressable>
      </Pressable>
    );
  },
);

export default React.memo(ListItem);

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 6,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: Colors.white,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Colors.borderColor,
    shadowColor: Colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  leftSide: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    color: Colors.black,
  },
  date: {
    fontSize: 12,
    color: Colors.gray,
  },
  time: {
    fontSize: 12,
    color: Colors.gray,
  },
  deleteButton: {
    padding: 10,
    borderRadius: 40,
    backgroundColor: Colors.red,
  },
  delete: {
    color: Colors.white,
    fontSize: 16,
  },
});
