import React from 'react';
import {Text, Pressable, StyleSheet, PressableProps} from 'react-native';
import Colors from '../constants/Colors';

type ButtonProps = PressableProps & {
  title: string;
  color?: string;
  onPress: () => void;
};

const Button = ({title, color, ...props}: ButtonProps) => {
  return (
    <Pressable
      style={[styles.container, {backgroundColor: color || Colors.green}]}
      {...props}>
      <Text style={styles.title}>{title}</Text>
    </Pressable>
  );
};

export default React.memo(Button);

const styles = StyleSheet.create({
  container: {
    padding: 16,
    borderRadius: 10,
    marginTop: 16,
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: Colors.white,
  },
});
