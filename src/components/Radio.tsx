import React from 'react';
import {Pressable, PressableProps, StyleSheet, Text, View} from 'react-native';
import Colors from '../constants/Colors';

type RadioProps = PressableProps & {
  title: string;
  isActive: boolean;
};

const Radio = ({title, isActive, ...props}: RadioProps) => (
  <Pressable style={styles.container} {...props}>
    <View style={styles.radioContainer}>
      {isActive && <View style={styles.radio} />}
    </View>
    <Text style={styles.title}>{title}</Text>
  </Pressable>
);

export default React.memo(Radio);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
  },
  radioContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    borderRadius: 50,
    padding: 2,
    marginRight: 12,
    borderColor: Colors.red,
    borderWidth: 2,
    width: 18,
    height: 18,
  },
  radio: {
    width: 11,
    height: 11,
    borderRadius: 10,
    borderColor: Colors.white,
    backgroundColor: Colors.red,
  },
  title: {
    fontSize: 16,
    color: Colors.black,
  },
});
