import * as React from 'react';
import {
  View,
  StyleSheet,
  PressableProps,
  TextStyle,
  ViewStyle,
  Pressable,
} from 'react-native';
import Modal from 'react-native-modal';
import Colors from '../constants/Colors';
import Dimensions from '../constants/Layout';

type Props = PressableProps & {
  isVisible?: boolean;
  setIsVisible?: any;
  modalStyle?: TextStyle | ViewStyle;
  modalViewStyle?: TextStyle | ViewStyle;
};

const SwipeableModal = (props: Props) => {
  return (
    <Modal
      isVisible={props.isVisible}
      swipeDirection="down"
      onSwipeComplete={() => {
        props.setIsVisible(false);
      }}
      animationIn={'slideInUp'}
      animationOut={'slideOutDown'}
      animationInTiming={700}
      animationOutTiming={700}
      onBackButtonPress={() => props.setIsVisible(false)}
      style={[styles.modal, props.modalStyle && props.modalStyle]}>
      <Pressable
        style={styles.modalTop}
        onPress={() => props.setIsVisible(false)}
      />
      <View
        style={[
          styles.modalView,
          props.modalViewStyle && props.modalViewStyle,
        ]}>
        {props.children}
      </View>
    </Modal>
  );
};

export default SwipeableModal;

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalTop: {
    height: Dimensions.window.height * 0.4,
  },
  modalView: {
    backgroundColor: Colors.white,
    height: Dimensions.window.height * 0.6,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 5,
    paddingHorizontal: 40,
  },
});
