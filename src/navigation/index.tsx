import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../screens/HomeScreen';
import AddScreen from '../screens/AddScreen';
import DetailScreen from '../screens/DetailScreen';

type RootStackParamList = {
  Home: undefined;
  Detail: {item: any};
  Add: undefined;
};

const RootStack = createNativeStackNavigator<RootStackParamList>();

const Navigation = () => {
  return (
    <RootStack.Navigator>
      <RootStack.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <RootStack.Screen
        name="Detail"
        component={DetailScreen}
        options={{headerTitle: ''}}
      />
      <RootStack.Screen
        name="Add"
        component={AddScreen}
        options={{headerTitle: ''}}
      />
    </RootStack.Navigator>
  );
};

export default Navigation;
