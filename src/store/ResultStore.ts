import {action, autorun, computed, makeAutoObservable, observable} from 'mobx';
import {Result} from '../types';

class ResultStore {
  results: Result[] = []; // Array of Result
  filterResults: Result[] = [];
  constructor() {
    makeAutoObservable(this, {
      results: observable,
      createResult: action,
      updateResult: action,
      deleteResult: action,
      sortByDate: computed,
      filterByDate: action,
    });
    autorun(() => {
      console.log('sorted', this.sortByDate);
    });
  }

  setResults(results: Result[]) {
    this.results = results;
  }

  getResults() {
    return this.results;
  }

  createResult(result: Result) {
    this.results.push(result);
  }

  updateResult(id: number, result: Result) {
    this.results[id] = result;
  }

  deleteResult(id: string) {
    this.results = this.results.filter(result => result.id !== id);
  }

  filterByDate(initialDate: Date, finalDate: Date) {
    return this.results.filter(result => {
      const date = new Date(result.date);
      return date >= initialDate && date <= finalDate;
    });
  }

  get sortByDate() {
    return this.results.slice().sort((a, b) => {
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    });
  }
}

export default new ResultStore();
