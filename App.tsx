import {StatusBar} from 'react-native';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import Navigation from './src/navigation';
import {Provider} from 'mobx-react';
import Store from './src/store';

export default function App() {
  return (
    <Provider {...Store}>
      <SafeAreaProvider>
        <NavigationContainer>
          <Navigation />
        </NavigationContainer>
        <StatusBar />
      </SafeAreaProvider>
    </Provider>
  );
}
